import axios from "axios";
import { API_ROOT } from "@/config";

const login = payload => {
  return axios.post(`${API_ROOT}/auth/login`, payload);
};

export { login };

import axios from "axios";
import { API_ROOT } from "@/config";

const registerUser = payload => {
  return axios.post(`${API_ROOT}/user/register`, payload);
};

const userList = () => {
  return axios.get(`${API_ROOT}/user`);
};

const getUser = userId => {
  return axios.get(`${API_ROOT}/user/${userId}`);
};

const uploadAvatar = (userId, payload) => {
  return axios.post(`${API_ROOT}/user/${userId}/avatar`, payload, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  });
};

const updateUser = (userId, payload) => {
  return axios.patch(`${API_ROOT}/user/${userId}`, payload);
};

export { registerUser, userList, getUser, uploadAvatar, updateUser };

import axios from "axios";
import { API_ROOT } from "@/config";

const praisesList = () => {
  return axios.get(`${API_ROOT}/praise`);
};

const addPraise = payload => {
  return axios.post(`${API_ROOT}/praise`, payload);
};

export { praisesList, addPraise };

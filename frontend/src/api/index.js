import * as auth from "./auth";
import * as praise from "./praise";
import * as user from "./user";

export default {
  auth,
  praise,
  user
};

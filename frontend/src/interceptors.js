import axios from "axios";
import store from "@/store";
import router from "@/router";
import { API_ROOT } from "@/config";

// Function found at https://stackoverflow.com/a/53294310
export function setupAxiosResponseInterceptor() {
  const interceptor = axios.interceptors.response.use(
    response => response,
    error => {
      if (error.response.status !== 401) {
        return Promise.reject(error);
      }

      /*
       * When response code is 401, try refreshing the token.
       * Eject the interceptor so that it doesn't cause a loop
       * in case token refresh causes the 401 response
       */
      axios.interceptors.response.eject(interceptor);

      const AUTHORIZATION = {
        headers: {
          Authorization: `Bearer ${store.getters.refreshToken}`
        }
      };

      return axios
        .post(`${API_ROOT}/auth/refresh-token`, null, AUTHORIZATION)
        .then(response => {
          const accessToken = response.data.access_token;
          store.commit("updateAccessToken", accessToken);
          error.response.config.headers[
            "Authorization"
          ] = `Bearer ${accessToken}`;
          return axios(error.response.config);
        })
        .catch(error => {
          store.commit("clearAccessToken");
          store.commit("clearRefreshToken");
          store.commit("clearUser");
          router.push("/").then(() => {
            console.log("Logging out user... Redirecting to login page");
          });
          return Promise.reject(error);
        })
        .finally(setupAxiosResponseInterceptor);
    }
  );
}

export function setupAxiosRequestInterceptor() {
  axios.interceptors.request.use(
    function(config) {
      if (
        !config.url.includes("/auth/login") &&
        !config.url.includes("/auth/refresh-token")
      ) {
        const token = store.getters.accessToken;
        if (token) {
          config.headers.Authorization = `Bearer ${token}`;
        }
      }
      return config;
    },
    function(err) {
      return Promise.reject(err);
    }
  );
}

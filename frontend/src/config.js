export const API_ROOT = "http://localhost:5000/v1";
export const DESIGNATIONS = [
  "CEO",
  "COO",
  "Intern",
  "Undesignated",
  "Operations Manager",
  "Pilot",
  "Team Leader",
  "Frontend Developer",
  "Backend Developer",
  "Fullstack Developer",
  "iOS Developer",
  "Android Developer",
  "Mobile App Developer"
];
export const BADGES = [
  {
    title: "Gem",
    subtitle: "Gem of a person",
    icon: "gem",
    color: "rgba(223,77,96,1)"
  },
  {
    title: "Medal",
    subtitle: "Stellar job and deserving a medal",
    icon: "medal",
    color: "rgb(248, 183, 75)"
  },
  {
    title: "Thinker",
    subtitle: "Deep thought resulting in well defined solutions",
    icon: "thinker",
    color: "rgb(189, 195, 199)"
  },
  {
    title: "Team Player",
    subtitle: "Collaborates across the board efficiently",
    icon: "teamplayer",
    color: "rgb(249, 195, 150)"
  },
  {
    title: "Punctual",
    subtitle: "Time is his ally",
    icon: "punctual",
    color: "rgb(140, 221, 254)"
  },
  {
    title: "Hard Worker",
    subtitle: "Works like the world is going to end",
    icon: "effort",
    color: "rgb(62, 89, 107)"
  },
  {
    title: "Crafty",
    subtitle: "Got a lot of tools in his belt",
    icon: "crafty",
    color: "rgb(221, 53, 46)"
  },
  {
    title: "Designer",
    subtitle: "Gives an artistic touch to everything",
    icon: "designer",
    color: "rgb(72, 128, 137)"
  },
  {
    title: "Executor",
    subtitle: "Gets the job done regardless of the circumstances",
    icon: "finisher",
    color: "rgb(57, 63, 74)"
  },
  {
    title: "On Fire",
    subtitle: "Pure energy spirit roaming in the halls",
    icon: "fire",
    color: "rgb(243, 112, 90)"
  },
  {
    title: "Goal Oriented",
    subtitle: "You shall not stand in the way.",
    icon: "focussed",
    color: "rgb(255, 112, 88)"
  },
  {
    title: "Machine",
    subtitle: "Consistent like a machine.",
    icon: "machine",
    color: "rgb(68, 130, 195)"
  },
  {
    title: "Heart",
    subtitle: "Spirit who shall humble you with kindness",
    icon: "heart",
    color: "rgb(255, 98, 67)"
  },
  {
    title: "Innovator",
    subtitle: "Out of the box thinker",
    icon: "innovative",
    color: "rgb(255, 209, 92)"
  },
  {
    title: "Mediator",
    subtitle: "Puts out fires like a firefighter",
    icon: "mediator",
    color: "rgb(59, 151, 211)"
  },
  {
    title: "Mentor",
    subtitle: "Your ever helpful guide",
    icon: "mentor",
    color: "rgb(78, 144, 30)"
  },
  {
    title: "Sneaky Surpriser",
    subtitle: "Ninja who surprises you with sudden moves",
    icon: "ninja",
    color: "rgb(119, 113, 140)"
  },
  {
    title: "Thanks",
    subtitle: "Spirit that helped you in the moment of need",
    icon: "thanks",
    color: "rgb(252, 193, 1)"
  },
  {
    title: "Good Job",
    subtitle: "Professional grade quality of work",
    icon: "thumbsup",
    color: "rgb(66, 166, 237)"
  },
  {
    title: "Spirit Uplifter",
    subtitle: "Feeling down? This is the person to talk to ...",
    icon: "uplifter",
    color: "rgb(240, 196, 25)"
  },
  {
    title: "Visionary",
    subtitle: "Mars is the next home. Thoughts of a visionary",
    icon: "visionary",
    color: "rgb(64, 89, 107)"
  }
];

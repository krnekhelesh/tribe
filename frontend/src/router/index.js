import Vue from "vue";
import VueRouter from "vue-router";

import LandingPage from "@/views/LandingPage.vue";

Vue.use(VueRouter);

// Some of the routes have level code-splitting. This generates
// a separate chunk (about.[hash].js) for this route which is
// lazy-loaded when the route is visited.

const routes = [
  {
    path: "/",
    name: "LandingPage",
    component: LandingPage,
    meta: { requiresAuth: false, showSidebar: false }
  },
  {
    path: "/home",
    name: "HomePage",
    meta: { requiresAuth: true, showSidebar: true },
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/HomePage.vue")
  },
  {
    path: "/team",
    name: "TeamPage",
    meta: { requiresAuth: true, showSidebar: true },
    component: () =>
      import(/* webpackChunkName: "team" */ "../views/TeamPage.vue")
  },
  {
    path: "/team/:userId",
    name: "UserProfilePage",
    meta: { requiresAuth: true, showSidebar: true },
    component: () =>
      import(/* webpackChunkName: "userprofile" */ "../views/ProfilePage")
  },
  {
    path: "/team/:userId/editProfile",
    name: "EditUserProfilePage",
    meta: { requiresAuth: true, showSidebar: true },
    component: () =>
      import(/* webpackChunkName: "userprofile" */ "../views/EditProfilePage")
  },
  {
    path: "/praises",
    name: "PraisesPage",
    meta: { requiresAuth: true, showSidebar: true },
    component: () =>
      import(/* webpackChunkName: "praises" */ "../views/PraisesPage")
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

router.beforeEach((to, from, next) => {
  const isLoggedIn = localStorage.tribeAccessToken;

  if (isLoggedIn && to.name === "LandingPage") {
    next({ name: "HomePage" });
    return;
  }

  if (to.matched.some(route => route.meta.requiresAuth)) {
    if (!isLoggedIn) next({ name: "LandingPage" });
    else next();
  } else {
    next();
  }
});

export default router;

const state = {
  user: {
    id: null,
    firstName: null,
    lastName: null,
    email: null,
    avatar: null
  }
};

const getters = {
  user: state => {
    return state.user;
  }
};

const mutations = {
  clearUser(state) {
    delete localStorage.tribeUser;
    state.user = {
      id: null,
      firstName: null,
      lastName: null,
      email: null,
      avatar: null
    };
  },
  updateUser(state, payload) {
    localStorage.setItem("tribeUser", JSON.stringify(payload));
    state.user = payload;
  }
};

export default {
  state,
  getters,
  mutations
};

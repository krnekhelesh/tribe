const state = {
  accessToken: null,
  refreshToken: null
};

const getters = {
  accessToken: state => {
    return state.accessToken;
  },
  refreshToken: state => {
    return state.refreshToken;
  }
};

const mutations = {
  clearAccessToken(state) {
    delete localStorage.tribeAccessToken;
    state.accessToken = null;
  },
  clearRefreshToken(state) {
    delete localStorage.tribeRefreshToken;
    state.refreshToken = null;
  },
  updateAccessToken(state, payload) {
    localStorage.setItem("tribeAccessToken", payload);
    state.accessToken = payload;
  },
  updateRefreshToken(state, payload) {
    localStorage.setItem("tribeRefreshToken", payload);
    state.refreshToken = payload;
  }
};

export default {
  state,
  getters,
  mutations
};

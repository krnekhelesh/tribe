const state = {
  notificationPayload: {
    message: null,
    code: null,
    timeout: 3000
  }
};

const getters = {
  notificationPayload: state => {
    return state.notificationPayload;
  }
};

const mutations = {
  updateNotificationPayload(state, payload) {
    state.notificationPayload = payload;
  }
};

export default {
  state,
  getters,
  mutations
};

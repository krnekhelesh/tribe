import Vue from "vue";
import Vuex from "vuex";

import notification from "@/store/notification";
import user from "@/store/user";
import session from "@/store/session";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    notification,
    user,
    session
  }
});

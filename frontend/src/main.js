import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import store from "./store";
import router from "./router";
import {
  setupAxiosResponseInterceptor,
  setupAxiosRequestInterceptor
} from "@/interceptors";

Vue.config.productionTip = false;

// Auto refresh access token if it has expired and perform failed request
// again if it had failed due to 401 access token expiry.
setupAxiosResponseInterceptor();

// Auto add authorization header to all requests except for whitelisted routes
setupAxiosRequestInterceptor();

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount("#app");

# Tribe

Tribe is a web application aimed at setting the right culture for a team.

It is currently in heavy development. Please check back later.

_Note: To all Skylark Drones employees, please keep away until told so. If you give this project time to develop, I
promise, you will be in for a huge surprise._

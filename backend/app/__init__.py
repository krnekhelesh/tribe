import mongoengine
from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from app.setup import setup_blueprints
from config import app_config


def create_app(config_name: str) -> Flask:
    app = Flask(__name__, instance_relative_config=False)

    # Load config for use by flask_restplus and flask_jwt_extended plugins
    app.config.from_object(app_config[config_name])

    # Load flask plugins
    CORS(app)
    JWTManager(app)

    # Connect to database
    mongoengine.connect(host=app.config["DATABASE_URI"])

    with app.app_context():
        setup_blueprints()

    return app

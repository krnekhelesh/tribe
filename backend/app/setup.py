from flask import current_app as app

from app.v1.blueprint import v1_blueprint


def setup_blueprints() -> None:
    app.register_blueprint(v1_blueprint)

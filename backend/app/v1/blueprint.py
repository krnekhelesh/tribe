from flask import Blueprint
from flask_restplus import Api

from app.v1.controllers.auth import api as auth_ns
from app.v1.controllers.hello import api as hello_ns
from app.v1.controllers.praise import api as praise_ns
from app.v1.controllers.tribe import api as tribe_ns
from app.v1.controllers.user import api as user_ns

v1_blueprint = Blueprint("v1_blueprint", __name__, url_prefix="/v1")

api = Api(
    v1_blueprint,
    version="1.0 Dev",
    title="Tribe REST APIs",
    contact_email="krnekhelesh@gmail.com",
    description=(
        f"Welcome to Tribe REST API Documentation!"
        f"<br><br>"
        f"<b>Warning:</b> This version of the DMS REST API is in heavy "
        f"development. Expect the request and response structure to change "
        f"over the course of time. Testing of the APIs have also not been "
        f"completed!"
    ),
)

api.namespaces = []
api.add_namespace(auth_ns, path="/auth")
api.add_namespace(hello_ns, path="/hello")
api.add_namespace(praise_ns, path="/praise")
api.add_namespace(tribe_ns, path="/tribe")
api.add_namespace(user_ns, path="/user")

from flask_restplus import fields, Model


def praise_model() -> Model:
    from app.v1.controllers.praise import api
    from app.v1.models.user import basic_user_model

    return api.model(
        "praise_model",
        {
            "id": fields.String(attribute="_id"),
            "badge": fields.String(),
            "author": fields.Nested(basic_user_model()),
            "awardee": fields.Nested(basic_user_model()),
            "created_on": fields.Date(),
            "title": fields.String(),
            "description": fields.String(),
            "votes": fields.List(fields.Nested(basic_user_model())),
        },
    )


def praises_list_model() -> Model:
    from app.v1.controllers.praise import api

    return api.model(
        "praises_list_model", {"praises": fields.List(fields.Nested(praise_model()))}
    )

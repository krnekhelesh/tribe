from flask_restplus import fields, Model


def basic_user_model() -> Model:
    from app.v1.controllers.user import api

    return api.model(
        "basic_user_model",
        {
            "id": fields.String(title="User ObjectId"),
            "first_name": fields.String(title="First Name"),
            "last_name": fields.String(title="Last Name"),
            "email": fields.String(title="Email Address"),
            "avatar": fields.String(title="Avatar (base64 encoded)"),
        },
    )


def user_model() -> Model:
    from app.v1.controllers.user import api

    return api.inherit(
        "user_models",
        basic_user_model(),
        {
            "designation": fields.List(fields.String(title="Designation")),
            "tagline": fields.String(title="Tagline"),
            "biography": fields.String(title="Biography"),
            "birthday": fields.Date(title="Birthday"),
            "phone_number": fields.String(title="Phone Number"),
            "twitter": fields.String(title="Twitter Handle"),
            "gitlab": fields.String(title="Gitlab Profile"),
            "linkedin": fields.String(title="LinkedIn Profile"),
            "tribe": fields.String(title="Tribe Id"),
        },
    )


def user_login_model() -> Model:
    from app.v1.controllers.user import api

    return api.inherit(
        "user_login_model",
        basic_user_model(),
        {"access_token": fields.String(), "refresh_token": fields.String()},
    )


def user_list_delegate() -> Model:
    from app.v1.controllers.user import api

    return api.inherit(
        "user_list_delegate",
        basic_user_model(),
        {"designation": fields.List(fields.String(title="Designation"))},
    )


def users_list_model() -> Model:
    from app.v1.controllers.user import api

    return api.model(
        "users_list_model", {"users": fields.List(fields.Nested(user_list_delegate()))}
    )

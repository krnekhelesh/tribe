from flask_restplus import fields, Model


def tribe_model() -> Model:
    from app.v1.controllers.tribe import api

    return api.model(
        "tribe",
        {
            "id": fields.String(title="Id"),
            "name": fields.String(title="Name", example="Docker Dev"),
            "goal": fields.String(title="Goal", example="Build container software"),
        },
    )

from http import HTTPStatus

from flask import request
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_jwt_identity,
)
from flask_restplus import Namespace, Resource, abort, marshal

import app.v1.models.user as um
from app.schema import User

api = Namespace("auth", description="Authentication API")

user_login_model = um.user_login_model()


@api.route("/login")
class LoginController(Resource):
    @api.response(403, "No user found with the specified email")
    @api.response(200, "User successfully authenticated", user_login_model)
    def post(self):
        email = request.json.get("email", "")
        password = request.json.get("password", "")

        user_obj = User.objects(email=email).first()

        if user_obj is None:
            abort(403, "User with the specified email not found")

        if not User.verify_password(password, user_obj.password):
            abort(403, "Incorrect password")

        user = {
            "id": user_obj.id,
            "avatar": user_obj.avatar,
            "first_name": user_obj.first_name,
            "last_name": user_obj.last_name,
            "designation": user_obj.designation,
            "email": user_obj.email,
            "tribe": user_obj.tribe.id,
            "access_token": create_access_token(identity=user_obj.email),
            "refresh_token": create_refresh_token(identity=user_obj.email),
        }

        return marshal(user, user_login_model), HTTPStatus.OK


@api.route("/refresh-token")
class RefreshToken(Resource):
    @jwt_refresh_token_required
    def post(self):
        user_email = get_jwt_identity()
        return {"access_token": create_access_token(identity=user_email)}, HTTPStatus.OK

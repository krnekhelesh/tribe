from http import HTTPStatus
from typing import Tuple, Dict

from flask import request
from flask_restplus import Resource, Namespace, abort, marshal
from mongoengine.errors import ValidationError, FieldDoesNotExist

import app.v1.models.tribe as tm
from app.schema import Tribe

api = Namespace("tribe", description="Tribe API")

tribe_model = tm.tribe_model()


@api.route("")
class TribeController(Resource):
    @api.response(201, "Tribe created successfully!")
    def post(self) -> Tuple[Dict, int]:
        """
        Create new tribe
        """
        try:
            tribe = Tribe(**request.json)
            tribe.save()
        except (ValidationError, FieldDoesNotExist) as error:
            abort(400, str(error))
        else:
            return {"message": "Tribe created successfully!"}, HTTPStatus.CREATED


@api.route("/<tribe_id>", endpoint="get_tribe")
class TribeClass(Resource):
    @api.response(200, "Tribe fetched successfully", tribe_model)
    def get(self, tribe_id: str) -> Tuple[Dict, int]:
        """
        Retrieves a specific tribe
        """
        tribe = Tribe.objects(id=tribe_id).first()

        if not tribe:
            abort(404, f"Tribe with id {tribe_id} is not found!")

        return marshal(tribe, tribe_model), HTTPStatus.OK

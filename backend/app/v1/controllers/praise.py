from http import HTTPStatus

from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restplus import Resource, Namespace, marshal, abort
from mongoengine.errors import ValidationError, FieldDoesNotExist

import app.v1.models.praise as pr
from app.schema import Praise, User

api = Namespace("praise", description="Praise API")

praises_list_model = pr.praises_list_model()


@api.route("")
class PraiseController(Resource):
    @api.response(200, "All praises fetched successfully", praises_list_model)
    @jwt_required
    def get(self):
        user = User.find_user_by_email(get_jwt_identity())
        praises = Praise.objects().aggregate(
            [
                {"$match": {"tribe": user.tribe.id}},
                {"$sort": {"created_on": -1}},
                {
                    "$lookup": {
                        "from": "user",
                        "let": {"praise_author": "$author"},
                        "pipeline": [
                            {"$match": {"$expr": {"$eq": ["$$praise_author", "$_id"]}}},
                            {
                                "$project": {
                                    "email": 1,
                                    "first_name": 1,
                                    "last_name": 1,
                                    "avatar": 1,
                                    "id": "$_id",
                                }
                            },
                        ],
                        "as": "author",
                    }
                },
                {"$unwind": {"path": "$author"}},
                {
                    "$lookup": {
                        "from": "user",
                        "let": {"praise_awardee": "$awardee"},
                        "pipeline": [
                            {
                                "$match": {
                                    "$expr": {"$eq": ["$$praise_awardee", "$_id"]}
                                }
                            },
                            {
                                "$project": {
                                    "email": 1,
                                    "first_name": 1,
                                    "last_name": 1,
                                    "avatar": 1,
                                    "id": "$_id",
                                }
                            },
                        ],
                        "as": "awardee",
                    }
                },
                {"$unwind": {"path": "$awardee"}},
                {
                    "$lookup": {
                        "from": "user",
                        "localField": "votes",
                        "foreignField": "_id",
                        "as": "votes",
                    }
                },
                {
                    "$addFields": {
                        "votes": {
                            "$map": {
                                "input": "$votes",
                                "as": "votesm",
                                "in": {
                                    "id": "$$votesm._id",
                                    "first_name": "$$votesm.first_name",
                                    "last_name": "$$votesm.last_name",
                                    "email": "$$votesm.email",
                                    "avatar": "$$votesm.avatar",
                                },
                            }
                        }
                    }
                },
            ]
        )

        praises_list = {"praises": []}
        while praises.alive:
            praises_list["praises"].append(praises.next())

        return marshal(praises_list, praises_list_model), HTTPStatus.OK

    @jwt_required
    def post(self):
        user = User.find_user_by_email(get_jwt_identity())
        request.json["tribe"] = user.tribe.id

        try:
            praise = Praise(**request.json)
            praise.save()
        except (ValidationError, FieldDoesNotExist) as error:
            abort(400, str(error))
        else:
            return {"message": "Praised added successfully"}, HTTPStatus.CREATED

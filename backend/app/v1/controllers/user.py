from base64 import b64encode
import os
from http import HTTPStatus
from typing import Tuple, Dict

from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restplus import Resource, Namespace, abort, marshal
from mongoengine.errors import ValidationError, FieldDoesNotExist, NotUniqueError

import app.v1.models.user as um
from app.schema import User

api = Namespace("user", description="User API")

users_list_model = um.users_list_model()
user_model = um.user_model()


@api.route("/register")
class RegisterUserController(Resource):
    @api.response(201, "User registered successfully")
    @jwt_required
    def post(self) -> Tuple[Dict, int]:
        """
        Register user
        """
        try:
            tribe = User.find_user_by_email(get_jwt_identity()).tribe
            request.json["password"] = User.hash_password("password")
            request.json["tribe"] = tribe
            user = User(**request.json)
            user.save()
        except (ValidationError, FieldDoesNotExist, NotUniqueError) as error:
            abort(400, str(error))
        except KeyError as error:
            abort(400, f"Mandatory key: {error} not supplied!")
        else:
            return {"message": "User registered successfully"}, HTTPStatus.CREATED


@api.route("")
class UsersListController(Resource):
    @api.response(200, "All users fetched successfully", users_list_model)
    @jwt_required
    def get(self):
        user = User.find_user_by_email(get_jwt_identity())
        users = User.objects(tribe=user.tribe)
        return marshal({"users": users}, users_list_model), HTTPStatus.OK


@api.route("/<user_id>")
class UserController(Resource):
    @api.response(200, "User fetched successfully", user_model)
    @api.response(404, "User will specified id cannot be found!")
    @jwt_required
    def get(self, user_id):
        user = User.objects(id=user_id).first()
        if not user:
            abort(404, "User with specified id cannot be found!")
        return marshal(user, user_model), HTTPStatus.OK

    @api.response(204, "User updated successfully")
    @jwt_required
    def patch(self, user_id):
        user = User.objects(id=user_id).first()

        if not user:
            abort(404, "User with specified id cannot be found!")
        elif user.email != get_jwt_identity():
            abort(405, "Not authorised to modify this user's profile!")

        try:
            user.modify(**request.json)
        except (ValidationError, FieldDoesNotExist, NotUniqueError) as error:
            abort(400, str(error))
        else:
            return marshal({}, {}), HTTPStatus.NO_CONTENT


@api.route("/<user_id>/avatar")
class UserAvatar(Resource):
    ALLOWED_EXTENSIONS = {".png", ".jpg", ".jpeg"}

    def allowed_file(self, filename):
        return os.path.splitext(filename.lower())[1] in self.ALLOWED_EXTENSIONS

    @jwt_required
    def post(self, user_id):
        user = User.objects(id=user_id).first()

        if not user:
            abort(404, "User with specified id cannot be found")
        elif user.email != get_jwt_identity():
            abort(405, "Unauthorised to modify this user's avatar!")
        elif "file" not in request.files:
            abort(400, "No avatar was uploaded!")
        elif not self.allowed_file(request.files["file"].filename):
            abort(400, "Only .png, .jpg and .jpeg extensions are allowed!")

        file = request.files["file"]
        if file:
            user.modify(
                **{
                    "avatar": (
                        f"data:image/{os.path.splitext(file.filename)[1][1:]};"
                        f"base64,{b64encode(file.read()).decode('utf-8')}"
                    )
                }
            )

from http import HTTPStatus
from typing import Tuple, Dict

from flask_restplus import Resource, Namespace

api = Namespace("hello", description="Hello API")


@api.route("")
class HelloWorld(Resource):
    @api.response(200, "Hello World Received!")
    def get(self) -> Tuple[Dict, int]:
        """
        Hello World Function
        """
        return {"message": "Hello! Tribe is alive!"}, HTTPStatus.OK

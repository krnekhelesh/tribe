import mongoengine
from flask import url_for


class Tribe(mongoengine.Document):
    name = mongoengine.StringField(required=True, unique=True)
    goal = mongoengine.StringField()

    @staticmethod
    def get_url(tribe_id):
        return url_for("v1_blueprint.get_tribe", id=tribe_id, _external=True)

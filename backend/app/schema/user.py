import mongoengine
from passlib.hash import sha512_crypt

from app.schema.tribe import Tribe
from app.utils.constants import USER_ROLES


class User(mongoengine.Document):
    avatar = mongoengine.StringField()
    first_name = mongoengine.StringField(
        required=True, unique_with=["tribe", "last_name"]
    )
    last_name = mongoengine.StringField(required=True)
    password = mongoengine.StringField(required=True)
    tribe = mongoengine.ReferenceField(Tribe, required=True)
    email = mongoengine.EmailField(required=True, unique=True)
    phone_number = mongoengine.StringField()
    joining_date = mongoengine.DateField()
    designation = mongoengine.ListField(
        mongoengine.StringField(choices=USER_ROLES), default=["Undesignated"]
    )
    birthday = mongoengine.DateField()

    biography = mongoengine.StringField()
    tagline = mongoengine.StringField()

    twitter = mongoengine.StringField()
    gitlab = mongoengine.URLField()
    linkedin = mongoengine.URLField()

    @classmethod
    def find_user_by_email(cls, email):
        return cls.objects(email=email).first()

    @classmethod
    def hash_password(cls, password):
        return sha512_crypt.hash(password)

    @classmethod
    def verify_password(cls, password, reference_password):
        return sha512_crypt.verify(password, reference_password)

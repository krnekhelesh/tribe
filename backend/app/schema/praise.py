from mongoengine import Document, ReferenceField, DateField, StringField, ListField

from app.schema import User, Tribe


class Praise(Document):
    badge = StringField()
    author = ReferenceField(User)
    awardee = ReferenceField(User)
    created_on = DateField()
    title = StringField()
    description = StringField()
    votes = ListField(ReferenceField(User))
    tribe = ReferenceField(Tribe)

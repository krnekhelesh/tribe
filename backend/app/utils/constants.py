USER_ROLES = [
    "CEO",
    "COO",
    "Intern",
    "Undesignated",
    "Operations Manager",
    "Pilot",
    "Team Leader",
    "Frontend Developer",
    "Backend Developer",
    "Fullstack Developer",
    "iOS Developer",
    "Android Developer",
    "Mobile App Developer",
]

import os
from datetime import timedelta


class Config:
    DEBUG = False

    # Disable Swagger Try it Out feature which will not work due to missing auth.
    SWAGGER_SUPPORTED_SUBMIT_METHODS = []

    # Minify JSON outputs by stripping whitespace and newlines
    # Solution found at https://stackoverflow.com/a/49145891
    RESTPLUS_JSON = {"indent": None, "separators": (",", ":")}

    # Flask JWT Extended Configuration
    JWT_ALGORITHM = "RS256"
    JWT_PRIVATE_KEY = open(os.getenv("JWT_SECRET_KEY")).read()
    JWT_PUBLIC_KEY = open(os.getenv("JWT_PUBLIC_KEY")).read()
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=15)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=30)
    PROPAGATE_EXCEPTIONS = True


class DevelopmentConfig(Config):
    DEBUG = True
    DATABASE_NAME = "dev_tribe"
    DATABASE_URI = f"mongodb://localhost:27017/{DATABASE_NAME}"


class TestingConfig(Config):
    TESTING = True
    DEBUG = True
    DATABASE_NAME = "test_tribe"
    DATABASE_URI = f"mongodb://localhost:27017/{DATABASE_NAME}"


class ProductionConfig(Config):
    DEBUG = False
    TESTING = False
    DATABASE_NAME = "tribe"
    DATABASE_URI = f"mongodb://localhost:27017/{DATABASE_NAME}"


app_config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig,
    "production": ProductionConfig,
}
